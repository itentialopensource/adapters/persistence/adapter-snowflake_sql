
## 0.1.2 [04-11-2023]

* Update adapter-utils version and baseunit test

See merge request itentialopensource/adapters/persistence/adapter-snowflake_sql!1

---

## 0.1.1 [04-04-2023]

* Bug fixes and performance improvements

See commit 79976f3

---
