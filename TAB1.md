# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Snowflake_sql System. The API that was used to build the adapter for Snowflake_sql is usually available in the report directory of this adapter. The adapter utilizes the Snowflake_sql API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Snowflake SQL adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Snowflake SQL. 

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
