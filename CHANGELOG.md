
## 0.3.4 [10-14-2024]

* Changes made at 2024.10.14_19:36PM

See merge request itentialopensource/adapters/adapter-snowflake_sql!12

---

## 0.3.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-snowflake_sql!10

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_17:39PM

See merge request itentialopensource/adapters/adapter-snowflake_sql!9

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_18:36PM

See merge request itentialopensource/adapters/adapter-snowflake_sql!8

---

## 0.3.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/persistence/adapter-snowflake_sql!7

---

## 0.2.3 [03-29-2024]

* Changes made at 2024.03.29_10:33AM

See merge request itentialopensource/adapters/persistence/adapter-snowflake_sql!6

---

## 0.2.2 [03-13-2024]

* Changes made at 2024.03.13_15:18PM

See merge request itentialopensource/adapters/persistence/adapter-snowflake_sql!5

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_12:46PM

See merge request itentialopensource/adapters/persistence/adapter-snowflake_sql!4

---

## 0.2.0 [01-03-2024]

* Adapter Migration

See merge request itentialopensource/adapters/persistence/adapter-snowflake_sql!3

---

## 0.1.2 [04-11-2023]

* Update adapter-utils version and baseunit test

See merge request itentialopensource/adapters/persistence/adapter-snowflake_sql!1

---

## 0.1.1 [04-04-2023]

* Bug fixes and performance improvements

See commit 79976f3

---
