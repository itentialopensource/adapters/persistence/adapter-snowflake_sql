# Snowflake_sql

Vendor: Snowflake
Homepage: https://www.snowflake.com/en/

Product: Snowflake SQL
Product Page: https://www.snowflake.com/en/

## Introduction
We classify Snowflake SQL into the Cloud domain as Snowflake SQL is a cloud-based data warehousing service providing scalable and efficient SQL querying capabilities for data analytics.

## Why Integrate
The Snowflake SQL adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Snowflake SQL. 

## Additional Product Documentation
The [API documents for Snowflake SQL](https://docs.snowflake.com/en/developer-guide/sql-api/index)
